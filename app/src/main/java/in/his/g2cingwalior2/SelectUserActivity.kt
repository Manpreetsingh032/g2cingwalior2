package `in`.his.g2cingwalior2

import `in`.his.g2cingwalior2.apiinterface.ApiService
import `in`.his.g2cingwalior2.apiinterface.RetroClient
import `in`.his.g2cingwalior2.dataModel.CheckUserLogin
import `in`.his.g2cingwalior2.dataModel.ResponseLogout
import `in`.his.g2cingwalior2.dataModel.ResponseManageSession
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_select_user.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SelectUserActivity : AppCompatActivity() {
    private var apiService: ApiService = RetroClient.getClient().create(ApiService::class.java)
    val c = this@SelectUserActivity
    private val handleSharedPreference: HandleSharedPreference by lazy { HandleSharedPreference(c) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_user)

        user1.setOnClickListener {
            checkUserLogin("user1")
        }
        user2.setOnClickListener {
            checkUserLogin("user2")
        }
        user3.setOnClickListener {
            checkUserLogin("user3")
        }
        user4.setOnClickListener {
            checkUserLogin("user4")
        }

        current_user.setOnClickListener {
            if (!handleSharedPreference.getUser().isNullOrEmpty()) {
                try {
                    startActivity(Intent(c, BarcodeActivity::class.java))
                    finish()
                } catch (e: Exception) {
                }
            } else {
                Toast.makeText(this, "Please Select a user", Toast.LENGTH_SHORT).show()
            }
        }

        logout.setOnClickListener {
            if (!handleSharedPreference.getUser().isNullOrEmpty()) {
                logoutUser(handleSharedPreference.getUser())
            }
        }
    }

    private fun checkUserLogin(s: String) {
        if (!handleSharedPreference.getUser()
                .isNullOrEmpty() && handleSharedPreference.getUser() == s
        ) {
            try {
                startActivity(Intent(c, BarcodeActivity::class.java))
                finish()
            } catch (e: Exception) {
            }
        } else {
            loginUser(s)
        }
    }

    private fun logoutUser(user: String?) {
        val call = apiService.LOGOUT_CALL(ApiService.user_name, user)

        call.enqueue(object : Callback<ResponseLogout> {
            override fun onResponse(
                call: Call<ResponseLogout>,
                response: Response<ResponseLogout>
            ) {
                if (response.isSuccessful && response.body() != null) {
                    if (response.body()!!.logout!!) {
                        handleSharedPreference.saveUser("")
                        Toast.makeText(c, "Logout Successful", Toast.LENGTH_SHORT)
                            .show()
                    }
                } else {
                    Toast.makeText(c, "Please retry something went wrong", Toast.LENGTH_SHORT)
                        .show()
                }

            }

            override fun onFailure(call: Call<ResponseLogout>, t: Throwable) {
                Toast.makeText(c, "Please retry something went wrong", Toast.LENGTH_SHORT).show()
            }

        })
    }

    private fun loginUser(s: String) {
        val call = apiService.MANAGE_SESSION_CALL(CheckUserLogin(s, ApiService.user_name))

        call.enqueue(object : Callback<ResponseManageSession> {
            override fun onResponse(
                call: Call<ResponseManageSession>,
                response: Response<ResponseManageSession>
            ) {
                if (response.isSuccessful && response.body() != null) {
                    if (response.body()!!.checkUser!!) {
                        try {
                            handleSharedPreference.saveUser(s)
                            startActivity(Intent(c, BarcodeActivity::class.java))
                            finish()
                        } catch (e: Exception) {
                        }
                    } else {
                        Toast.makeText(
                            c,
                            "User is already login in another device",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseManageSession>, t: Throwable) {
                Toast.makeText(c, "Please retry something went wrong", Toast.LENGTH_SHORT).show()
            }

        })
    }


}