package `in`.his.g2cingwalior2

import `in`.his.g2cingwalior2.apiinterface.ApiService
import `in`.his.g2cingwalior2.apiinterface.RetroClient
import `in`.his.g2cingwalior2.dataModel.*
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.cart_item.*
import kotlinx.android.synthetic.main.cart_list.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CartActivity : AppCompatActivity() {
    private var apiService: ApiService = RetroClient.getClient().create(ApiService::class.java)
    lateinit var cartListAdapter: CartListSkuAdapter
    var cartListItem = ArrayList<CartListItem>()
    var skuListItem = ArrayList<SkuListItem>()
    private val handleSharedPreference: HandleSharedPreference by lazy { HandleSharedPreference(this) }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.cart_list)

        cartListAdapter = CartListSkuAdapter()
        val linearLayoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rv_cart_list.layoutManager = linearLayoutManager
        rv_cart_list.adapter = cartListAdapter
        cartListAdapter.deleteClick = object : DeleteClick {
            override fun clickDelete(id: String, position: Int) {

            }

            override fun clickSku(sku: String?) {
                startActivity(
                    Intent(
                        this@CartActivity,
                        DeleteBarcodeListItemActivity::class.java
                    ).putExtra("SKU", sku)
                )
                finish()
            }
        }
        getCartItems()

        submit_cart.setOnClickListener {
            val alertDialog: AlertDialog.Builder = AlertDialog.Builder(this@CartActivity)
            alertDialog.setMessage("Are you sure you want to submit all items?")
            alertDialog.setPositiveButton(
                "yes"
            ) { _, _ ->
                submitCart()
            }
            alertDialog.setNegativeButton(
                "No"
            ) { _, _ ->

            }
            val alert: AlertDialog = alertDialog.create()
            alert.setCanceledOnTouchOutside(false)
            alert.show()

        }
    }


    override fun onBackPressed() {
        try {
            startActivity(Intent(this, BarcodeActivity::class.java))
        } catch (e: Exception) {
        }
        super.onBackPressed()
    }

    private fun submitCart() {
        val list = ArrayList<UpdateData>()
        list.clear()
        cartListItem.forEach {
            list.add(
                UpdateData(
                    it.hsn,
                    it.cartonGrossWeight.toString(),
                    it.noOfPcs.toString(),
                    it.nameOfItem,
                    it.packaging.toString(),
                    it.barcode,
                    it.userName,
                    it.perPcsWeight.toString()
                )
            )
        }

        val call = apiService.SUBMIT_CART_LIST_PRODUCTION_CALL(ListProduction(list))
        call.enqueue(object : Callback<ResponseInsertProduction> {
            override fun onResponse(
                call: Call<ResponseInsertProduction>,
                response: Response<ResponseInsertProduction>
            ) {
                if (response.isSuccessful && response.body() != null) {
                    if (response.body()!!.message!!.isNotEmpty()) {
                        getCartItems()
                    }
                } else {
                    Toast.makeText(
                        this@CartActivity,
                        "There is Some Problem With Connection",
                        Toast.LENGTH_SHORT
                    ).show()
                    getCartItems()
                }
            }

            override fun onFailure(call: Call<ResponseInsertProduction>, t: Throwable) {
                if (t.toString().contains("java.io.EOFException")) {
                    print(t)
                } else {
                    try {
                        getCartItems()
                        Toast.makeText(
                            this@CartActivity,
                            "There is Some Problem With Connection",
                            Toast.LENGTH_SHORT
                        ).show()

                    } catch (e: Exception) {
                    }
                }
            }

        })

    }

    private fun getCartItems() {
        val call = apiService.GET_CART_LIST("in", handleSharedPreference.getUser(),ApiService.user_name)
        call.enqueue(object : Callback<ResponseCartList> {
            override fun onResponse(
                call: Call<ResponseCartList>,
                response: Response<ResponseCartList>
            ) {
                if (response.isSuccessful && response.body() != null) {
                    cartListItem.clear()
                    skuListItem.clear()
                    if (response.body()!!.cartList!!.isNotEmpty()) {
                        cartListItem = response.body()!!.cartList as ArrayList<CartListItem>
                        val a = response.body()!!.cartList as ArrayList<CartListItem>
                        val d = a.distinctBy { s -> s.nameOfItem }
                        skuListItem.clear()
                        d.forEach {
                            val c = a.count { s -> s.nameOfItem == it.nameOfItem }
                            skuListItem.add(
                                SkuListItem(
                                    it.date,
                                    it.hsn,
                                    it.userName,
                                    it.packaging,
                                    it.type,
                                    it.perPcsWeight,
                                    it.cartonGrossWeight,
                                    it.qty,
                                    it.nameOfItem,
                                    it.noOfPcs,
                                    it.id,
                                    it.barcode,
                                    it.status,
                                    c.toString()
                                )
                            )
                            print(c)
                        }
                    }
                    cartListAdapter.setList(skuListItem)
                }
            }

            override fun onFailure(call: Call<ResponseCartList>, t: Throwable) {
                if (t.toString().contains("java.io.EOFException")) {
                    print(t)
                } else {
                    try {
                        Toast.makeText(
                            this@CartActivity,
                            "There is Some Problem With Connection",
                            Toast.LENGTH_SHORT
                        ).show()
                    } catch (e: Exception) {
                    }
                }
            }
        })
    }

}