package `in`.his.g2cingwalior2

import `in`.his.g2cingwalior2.apiinterface.ApiService
import `in`.his.g2cingwalior2.apiinterface.RetroClient
import `in`.his.g2cingwalior2.dataModel.InsertCartItem
import `in`.his.g2cingwalior2.dataModel.ProductDetailsItem
import `in`.his.g2cingwalior2.dataModel.ResponseInsertProduction
import `in`.his.g2cingwalior2.dataModel.ResponseProductList
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.dialog.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SubmitActivity : AppCompatActivity() {
    private var apiService: ApiService = RetroClient.getClient().create(ApiService::class.java)
    private var barcode: String? = ""
    var data: ProductDetailsItem? = ProductDetailsItem()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog)

        barcode = intent.extras?.get("barcodeData")?.toString()

        if (barcode != null) {
            getBarcodeDetail()
        } else {
            Toast.makeText(this, "Barcode Not Scanned Properly,Scan Again", Toast.LENGTH_SHORT)
                .show()
            openActivity()

        }

        submit_btn.setOnClickListener {
            submitProduct()
        }

        cancel_btn.setOnClickListener {
            openActivity()
        }

        cart.setOnClickListener {
            startActivity(Intent(this, CartActivity::class.java))
            finish()
        }
    }

    private fun openActivity() {
        try {
            startActivity(Intent(this, BarcodeActivity::class.java))
            finish()
        } catch (e: Exception) {

        }
    }

    private fun getBarcodeDetail() {
        try {
            val call = apiService.PRODUCT_LIST_CALL(barcode, ApiService.user_name)
            call.enqueue(object : Callback<ResponseProductList> {
                override fun onFailure(call: Call<ResponseProductList>, t: Throwable) {
                    print(t)
                }

                override fun onResponse(
                    call: Call<ResponseProductList>,
                    response: Response<ResponseProductList>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        when {
                            response.body()!!.productDetails.isNullOrEmpty() -> {
                                Toast.makeText(
                                    this@SubmitActivity,
                                    "Barcode Not Found",
                                    Toast.LENGTH_SHORT
                                ).show()
                                openActivity()
                            }
                            else -> {
                                data = response.body()!!.productDetails?.get(0)
                                if (data != null) {
                                    item_barcode.text = data!!.barcode
                                    item_name.text = data!!.nameOfItem
                                    pieces.text = data!!.numPcs.toString()
                                    packaging.text = data!!.packaging.toString()
                                    carton_gross_weight.text = data!!.cartonGrossWeight.toString()
                                    hsn.text = data!!.hsn.toString()
                                    per_pcs_wt.text = data!!.perPcsWeight.toString()
                                }
                            }
                        }
                    }
                }
            })
        } catch (e: Exception) {
            Toast.makeText(
                this@SubmitActivity,
                "Please verify again and resubmit",
                Toast.LENGTH_SHORT
            )
                .show()
            openActivity()
        }
    }

    private fun submitProduct() {
        try {
            if (data != null) {
                val call = apiService.INSERT_PRODUCTION_CART_CALL(
                    InsertCartItem(
                        null,
                        data!!.hsn.toString(),
                        data!!.cartonGrossWeight,
                        data!!.userName,
                        1,
                        data!!.nameOfItem,
                        data!!.numPcs!!.toDouble().toInt(),
                        data!!.packaging, "in", data!!.barcode, data!!.perPcsWeight, null, null
                    )
                )

                call.enqueue(object : Callback<ResponseInsertProduction> {
                    override fun onResponse(
                        call: Call<ResponseInsertProduction>,
                        response: Response<ResponseInsertProduction>
                    ) {
                        if (response.isSuccessful) {
                            if (response.body() != null) {
                                if (response.body()!!.message.isNullOrEmpty()) {
                                    Toast.makeText(
                                        this@SubmitActivity,
                                        "Please verify again and resubmit",
                                        Toast.LENGTH_SHORT
                                    )
                                        .show()
                                    openActivity()

                                } else {
                                    if (response.body()!!.message == "Successful") {
                                        Toast.makeText(
                                            this@SubmitActivity,
                                            "${response.body()!!.message}",
                                            Toast.LENGTH_SHORT
                                        )
                                            .show()
                                        openActivity()
                                    } else {
                                        Toast.makeText(
                                            this@SubmitActivity,
                                            "Already scanned, scan another item",
                                            Toast.LENGTH_SHORT
                                        )
                                            .show()
                                        openActivity()
                                    }
                                }
                            }
                        }
                    }

                    override fun onFailure(call: Call<ResponseInsertProduction>, t: Throwable) {
                        Toast.makeText(
                            this@SubmitActivity,
                            "Please Check Your Connection and resubmit",
                            Toast.LENGTH_SHORT
                        ).show()
                        openActivity()
                    }
                })

            } else {
                Toast.makeText(
                    this@SubmitActivity,
                    "Please verify again and resubmit",
                    Toast.LENGTH_SHORT
                )
                    .show()
                openActivity()
            }
        } catch (e: Exception) {
            print(e)
            openActivity()
        }
    }
}