package in.his.g2cingwalior2.apiinterface;

import in.his.g2cingwalior2.dataModel.CheckUserLogin;
import in.his.g2cingwalior2.dataModel.Password;
import in.his.g2cingwalior2.dataModel.InsertCartItem;
import in.his.g2cingwalior2.dataModel.ListProduction;
import in.his.g2cingwalior2.dataModel.ResponseCartList;
import in.his.g2cingwalior2.dataModel.ResponseInsertProduction;
import in.his.g2cingwalior2.dataModel.ResponseLogout;
import in.his.g2cingwalior2.dataModel.ResponseManageSession;
import in.his.g2cingwalior2.dataModel.ResponseProductList;
import in.his.g2cingwalior2.dataModel.UpdateData;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiService {
    String user_name = "warehouse2";
    /*http://192.168.43.106:8082/login*/

    @GET("getLogin/")
    Call<Password> PASSWORD_CALL(@Query("user_name") String user_name,
                                 @Query("password") String password);

    @GET("getProductBarcodeList/")
    Call<ResponseProductList> PRODUCT_LIST_CALL(@Query("barcode") String barcode,
                                                @Query("user_name") String user_name);

    @POST("insertAddProduct/")
    Call<ResponseInsertProduction> PRODUCT_INSERT_PRODUCTION(@Body UpdateData updateData);

    @GET("getCartList/")
    Call<ResponseCartList> GET_CART_LIST(@Query("type") String type,
                                         @Query("user_id") String user_id, @Query("user_name") String user_name);

    @POST("addProductionCart/")
    Call<ResponseInsertProduction> INSERT_PRODUCTION_CART_CALL(@Body InsertCartItem insertCartItem);

    @POST("deleteFromCart/")
    Call<ResponseInsertProduction> DELETE_FROM_CART_CALL(@Query("id") long id);

    @POST("addProductionByList/")
    Call<ResponseInsertProduction> SUBMIT_CART_LIST_PRODUCTION_CALL(@Body ListProduction listProduction);

    @POST("manageSession/")
    Call<ResponseManageSession> MANAGE_SESSION_CALL(@Body CheckUserLogin checkUserLogin);

    @GET("logout/")
    Call<ResponseLogout> LOGOUT_CALL(@Query("warehouses") String warehouses,
                                     @Query("user_name") String user_name);
}
