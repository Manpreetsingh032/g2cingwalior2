package in.his.g2cingwalior2.apiinterface;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetroClient {

    //http://192.168.0.108:8082/getProduct/wmsweb-0.0.1-SNAPSHOT/

    private static Retrofit retrofit = null;
        private static final String BASE_URL = "http://18.118.36.18:9080//api/";
//    private static final String BASE_URL = "http://192.168.1.36:9080/api/";

    //Todo - change the server address
    public static Retrofit getClient() {
        OkHttpClient client = new OkHttpClient.Builder().build();
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
        }
        return retrofit;
    }
}