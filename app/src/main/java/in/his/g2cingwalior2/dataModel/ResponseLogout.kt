package `in`.his.g2cingwalior2.dataModel

import com.google.gson.annotations.SerializedName

data class ResponseLogout(

    @field:SerializedName("logout")
    val logout: Boolean? = null
)
