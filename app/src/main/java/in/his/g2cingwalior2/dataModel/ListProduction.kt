package `in`.his.g2cingwalior2.dataModel

import com.google.gson.annotations.SerializedName

data class ListProduction(

    @field:SerializedName("listProduction")
    val productionList: List<UpdateData>? = null

)
