package `in`.his.g2cingwalior2.dataModel

import com.google.gson.annotations.SerializedName

data class ResponseManageSession(

	@field:SerializedName("checkUser")
	val checkUser: Boolean? = null
)
